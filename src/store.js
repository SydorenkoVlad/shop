import React, { createContext, useReducer } from 'react';

const initialState = {
  cartItems: [],
  isLoading: false,
};

const store = createContext(initialState);
const { Provider } = store;

const StateProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'setIsLoading':
        const isLoading = action.payload;
        return { ...state, isLoading };
      case 'setCartItems':
        const data = action.payload;
        return { ...state, cartItems: data };
      default:
        throw new Error();
    }
  }, initialState);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider };
