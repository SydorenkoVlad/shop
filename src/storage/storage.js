import { AsyncStorage } from './async-storage';

export async function initializeLocalStorage() {
  const items = [
    {
      id: 53,
      price: 321.23,
      name: 'Widget Adapter',
      description: 'Lorem Ipsum for Widget Adapter',
      image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
    },
    {
      id: 54,
      price: 321.23,
      name: 'Decorator',
      description: 'Lorem Ipsum for Decorator',
      image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
    },
    {
      id: 55,
      price: 360.23,
      name: 'Api',
      description: 'Lorem Ipsum for Api',
      image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
    },
    {
      id: 56,
      price: 350.23,
      name: 'Fix',
      description: 'Lorem Ipsum for Fix',
      image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
    },
  ];

  const cartItems = [
    {
      item_id: 54,
      name: 'Decorator',
      quantity: 2,
      price: 321.23,
      image:
        'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png',
    },
  ];

  await save('items', items);
  await save('cartItems', cartItems);
}

export async function load(key) {
  try {
    const almostThere = await AsyncStorage.getItem(key);
    return JSON.parse(almostThere);
  } catch {
    return null;
  }
}

export async function save(key, value) {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value));
    return true;
  } catch {
    return false;
  }
}

export async function clear() {
  try {
    await AsyncStorage.clear();
  } catch {}
}

export const apiMock = {
  getItems: async () => {
    const items = await load('items');
    return items;
  },
  getAllCartItems: async () => {
    const cartItems = await load('cartItems');
    return cartItems;
  },
  getItemDetail: async id => {
    const items = await load('items');
    const itemDetail = items.filter(item => item.id === id)[0];
    return itemDetail;
  },
  addItemToCart: async (id, quantity) => {
    const items = await load('items');
    const cartItems = await load('cartItems');

    const itemAlreadyInCartIndex = cartItems.findIndex(
      cartItem => cartItem.item_id === id,
    );

    if (itemAlreadyInCartIndex !== -1) {
      const currentItem = cartItems[itemAlreadyInCartIndex];
      cartItems[itemAlreadyInCartIndex] = {
        ...currentItem,
        quantity: currentItem.quantity + quantity,
      };
      await save('cartItems', cartItems);
    } else {
      const itemData = items.find(item => item.id === id);
      const { name, price, image } = itemData;
      const newItemForCart = { item_id: id, name, price, image, quantity };
      const newCartItems = [...cartItems, newItemForCart];
      await save('cartItems', newCartItems);
    }
  },
  updateItemInCart: async (id, quantity) => {
    const cartItems = await load('cartItems');

    const itemAlreadyInCartIndex = cartItems.findIndex(
      cartItem => cartItem.item_id === id,
    );

    const currentItem = cartItems[itemAlreadyInCartIndex];
    cartItems[itemAlreadyInCartIndex] = {
      ...currentItem,
      quantity,
    };
    await save('cartItems', cartItems);
  },
  resetCart: async () => {
    await save('cartItems', []);
  },
  deleteItemFromCart: async id => {
    const cartItems = await load('cartItems');
    const newCartItems = cartItems.filter(cartItem => cartItem.item_id !== id);

    await save('cartItems', newCartItems);
  },
};
