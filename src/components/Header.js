import {
  SafeAreaView,
  Pressable,
  Text,
  View,
  StyleSheet,
  Platform,
} from 'react-native';
import React from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export const Header = () => {
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <Pressable
        onPress={() => console.log('press on Burger')}
        style={styles.flex1}>
        <FontAwesome name="bars" size={28} color="black" />
      </Pressable>

      <View style={styles.flex5}>
        <Text style={styles.boldText}>bagzz</Text>
      </View>

      <View>
        <FontAwesome name="user-circle-o" size={28} color="black" />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeAreaContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 15,
    marginBottom: 15,
    marginTop: Platform.OS === 'android' ? 15 : 0,
  },
  flex1: { flex: 1 },
  flex5: { flex: 5 },
  boldText: { fontWeight: 'bold' },
});
