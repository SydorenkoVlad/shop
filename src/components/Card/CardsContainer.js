import React from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';

import { Card } from './Card';

export const CardsContainer = ({ items }) => {
  return (
    <ScrollView
      bounces={true}
      bouncesZoom={true}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      style={styles.scrollContainer}>
      <View style={styles.container}>
        {items.map((item, index) => (
          <Card key={item.name + index} data={item} />
        ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginHorizontal: 8,
    paddingBottom: 16,
  },
  scrollContainer: { flex: 1, marginTop: 15 },
});
