import React, { useContext } from 'react';
import {
  Pressable,
  Text,
  Image,
  StyleSheet,
  Platform,
  Dimensions,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';
import apiService from '../../services/api.service';
import { store } from '../../store';

const DELTA_PADDING_FOR_CARDS = 20;

export const Card = ({ data }) => {
  const navigation = useNavigation();

  const { id, name, price, image } = data;

  const { dispatch } = useContext(store);

  const shopNowPress = async () => {
    try {
      await apiService.addItemToCart(id, 1, dispatch);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Pressable
      style={styles.cardContainer}
      onPress={() => navigation.navigate('ItemDetail', { id })}>
      <Pressable style={styles.likeButton}>
        <FontAwesome name="heart-o" size={22} color="black" />
      </Pressable>
      <Image source={{ uri: image }} style={styles.imageStyles} />
      <Text style={styles.itemTitle} numberOfLines={1}>
        {name}
      </Text>
      <Text style={styles.priceTextStyle}>{`${price} $`}</Text>
      <Pressable onPress={shopNowPress} style={styles.shopNowButton}>
        <Text style={styles.whiteText}>SHOP NOW</Text>
      </Pressable>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: '#E0E0E0',
    marginVertical: 10,
    marginHorizontal: 3,
    paddingHorizontal: 5,
    width: Dimensions.get('window').width / 2 - DELTA_PADDING_FOR_CARDS,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: Platform.OS === 'android' ? 1 : 12,
    },
    shadowOpacity: Platform.OS === 'android' ? 0.22 : 0.58,
    shadowRadius: Platform.OS === 'android' ? 2.22 : 16.0,
    elevation: Platform.OS === 'android' ? 3 : 24,
  },
  likeButton: {
    position: 'absolute',
    top: 6,
    right: 6,
  },
  imageStyles: {
    width: 100,
    height: 120,
    alignSelf: 'center',
    marginVertical: 10,
  },
  itemTitle: {
    fontSize: 18,
    alignSelf: 'center',
  },
  shopNowButton: {
    backgroundColor: 'black',
    alignItems: 'center',
    width: '70%',
    paddingVertical: 10,
    marginVertical: 20,
    alignSelf: 'center',
  },
  priceTextStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 15,
  },
  whiteText: { color: 'white' },
});
