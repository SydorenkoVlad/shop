import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export const NoItemsInCart = () => (
  <View style={styles.container}>
    <Text>No items in cart yet</Text>
  </View>
);

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
});
