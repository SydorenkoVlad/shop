import React from 'react';
import { View, Image, Text, StyleSheet, Pressable } from 'react-native';
import { CountPicker } from './CountPicker';
import { useNavigation } from '@react-navigation/native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import apiService from '../../services/api.service';

export const CartItem = ({ isLast, itemParams, dispatch }) => {
  const { image, name, price, quantity, item_id: id } = itemParams;

  const navigation = useNavigation();

  const deleteItemFromCart = async () => {
    await apiService.deleteItemFromCart(id, dispatch);
  };

  return (
    <Pressable
      style={[styles.cartContainer, !isLast && styles.bottomBorder]}
      onPress={() => navigation.navigate('ItemDetail', { id })}>
      <View>
        <Image source={{ uri: image }} style={styles.imgStyle} />
        <CountPicker initialValue={quantity} id={id} />
      </View>
      <View style={styles.itemInfoContainer}>
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.priceTextStyle}>{`${price} $`}</Text>
      </View>
      <Pressable style={styles.deleteIcon} onPress={deleteItemFromCart}>
        <FontAwesome name="times" size={25} color="grey" />
      </Pressable>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  cartContainer: { flexDirection: 'row', paddingVertical: 10 },
  imgStyle: { width: 80, height: 80, alignSelf: 'center' },
  itemInfoContainer: { marginLeft: 30 },
  title: { fontSize: 18, fontWeight: '500', marginBottom: 10 },
  priceTextStyle: { fontSize: 20, fontWeight: 'bold', marginTop: 15 },
  bottomBorder: { borderBottomColor: 'black', borderBottomWidth: 1 },
  deleteIcon: { position: 'absolute', right: 0, top: 5 },
});
