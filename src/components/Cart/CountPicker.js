import React, { useContext } from 'react';
import { View, Pressable, Text, StyleSheet } from 'react-native';
import apiService from '../../services/api.service';
import { store } from '../../store';
import { debounce } from '../../utils';

export const CountPicker = ({ initialValue, id }) => {
  const [count, setCount] = React.useState(1);

  const { dispatch } = useContext(store);

  const debouncedUpdate = debounce(
    props => apiService.updateItemInCart(props),
    500,
  );

  React.useEffect(() => {
    setCount(initialValue);
  }, [initialValue]);

  const decreaseCount = async () => {
    const newCount = count - 1;

    if (!newCount) {
      await apiService.deleteItemFromCart(id, dispatch);
      return;
    }

    setCount(newCount);
    debouncedUpdate({ id, quantity: newCount, dispatch });
  };

  const increaseCount = () => {
    const newCount = count + 1;
    setCount(newCount);
    debouncedUpdate({ id, quantity: newCount, dispatch });
  };

  return (
    <View style={styles.container}>
      <Pressable
        style={[styles.pickerItemContainer, styles.bgBlack]}
        onPress={decreaseCount}>
        <Text style={[styles.boldText, styles.whiteText]}>-</Text>
      </Pressable>
      <View style={[styles.pickerItemContainer, styles.verticalBlackBorders]}>
        <Text style={styles.boldText}>{count}</Text>
      </View>
      <Pressable
        style={[styles.pickerItemContainer, styles.bgBlack]}
        onPress={increaseCount}>
        <Text style={[styles.boldText, styles.whiteText]}>+</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flexDirection: 'row', marginVertical: 10 },
  pickerItemContainer: {
    width: 40,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bgBlack: { backgroundColor: 'black' },
  boldText: { fontWeight: 'bold' },
  whiteText: { color: 'white' },
  verticalBlackBorders: {
    borderColor: 'black',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
  },
});
