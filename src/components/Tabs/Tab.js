import React from 'react';
import { Pressable, StyleSheet } from 'react-native';
import { Badge } from './Badge';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export const Tab = ({ name, pressFunc, isActive, badgeCount }) => (
  <Pressable style={styles.container} key={name} onPress={pressFunc}>
    {badgeCount && <Badge badgeCount={badgeCount} isActive={isActive} />}
    <FontAwesome
      name={name}
      size={isActive ? 36 : 24}
      color={isActive ? 'blue' : 'black'}
    />
  </Pressable>
);

const styles = StyleSheet.create({
  container: { padding: 20 },
});
