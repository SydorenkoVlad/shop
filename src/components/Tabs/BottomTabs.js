import React, { useContext } from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import { store } from '../../store';
import { Tab } from './Tab';

const calculateItemsInCart = cartItems =>
  cartItems.reduce((prev, current) => prev + current.quantity, 0);

export const BottomTabs = ({ navigation }) => {
  const [activeTab, setActiveTab] = React.useState('home');

  const {
    state: { cartItems },
  } = useContext(store);

  const badgeCount = cartItems?.length ? calculateItemsInCart(cartItems) : null;

  return (
    <View style={styles.tabsContainer}>
      <Tab
        name="home"
        isActive={activeTab === 'home'}
        pressFunc={() => {
          setActiveTab('home');
          navigation.navigate('Home');
        }}
      />
      <Tab
        name="search"
        isActive={activeTab === 'search'}
        pressFunc={() => {
          setActiveTab('search');
          navigation.navigate('Search');
        }}
      />
      <Tab
        name="heart"
        isActive={activeTab === 'heart'}
        pressFunc={() => {
          setActiveTab('heart');
          navigation.navigate('Like');
        }}
      />
      <Tab
        name="shopping-cart"
        isActive={activeTab === 'shopping-cart'}
        badgeCount={badgeCount}
        pressFunc={() => {
          setActiveTab('shopping-cart');
          navigation.navigate('Cart');
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  tabsContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 30,
    paddingBottom: Platform.OS === 'ios' ? 20 : 0,
  },
});
