import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export const Badge = ({ badgeCount, isActive }) => {
  return (
    <View style={[styles.circle, !isActive && styles.notActiveCircle]}>
      <Text style={styles.text}>{badgeCount}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  circle: {
    position: 'absolute',
    top: 17,
    right: 13,
    backgroundColor: 'black',
    zIndex: 1000,
    borderRadius: 18 / 2,
    width: 18,
    height: 18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  notActiveCircle: {
    backgroundColor: 'red',
    right: 4,
  },
  text: { color: 'white', alignSelf: 'center' },
});
