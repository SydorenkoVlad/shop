import React, { useContext } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { MainNavigator } from './main-navigator';
import { store } from '../store';
import { initializeLocalStorage, load } from '../storage';
import apiService from '../services/api.service';

const RootStack = createNativeStackNavigator();

const RootNavigation = () => {
  const { dispatch } = useContext(store);

  const setupStorage = async () => {
    const items = await load('items');
    !items && (await initializeLocalStorage());
  };

  const getCartItems = React.useCallback(async () => {
    try {
      const data = await apiService.getAllCartItems();
      dispatch({ type: 'setCartItems', payload: data });
    } catch (err) {
      console.log('ERRRRRORR! ', err.message);
    }
  }, [dispatch]);

  const initData = React.useCallback(async () => {
    dispatch({ type: 'setIsLoading', payload: true });

    !apiService.isRealAPI && (await setupStorage());
    await getCartItems();

    dispatch({ type: 'setIsLoading', payload: false });
  }, [dispatch, getCartItems]);

  React.useEffect(() => {
    initData();
  }, [initData]);

  return (
    <RootStack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <RootStack.Screen name="Main" component={MainNavigator} />
    </RootStack.Navigator>
  );
};

export default RootNavigation;
