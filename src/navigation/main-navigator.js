import React, { useContext } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { store } from '../store';
import apiService from '../services/api.service';
import { Home, Cart, Search, Like, ItemDetail } from '../pages';
import { BottomTabs, Header } from '../components';

const Tab = createBottomTabNavigator();

export function MainNavigator() {
  const globalState = useContext(store);
  const { dispatch } = globalState;

  const getCartItems = React.useCallback(async () => {
    try {
      const data = await apiService.getAllCartItems();
      dispatch({ type: 'setCartItems', payload: data });
    } catch (err) {
      console.log('ERRRRRORR! ', err.message);
    }
  }, [dispatch]);

  React.useEffect(() => {
    getCartItems();
  }, [getCartItems]);

  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBar={props => <BottomTabs {...props} />}
      screenOptions={{ header: () => <Header /> }}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Cart" component={Cart} />
      <Tab.Screen name="Search" component={Search} />
      <Tab.Screen name="Like" component={Like} />
      <Tab.Screen name="ItemDetail" component={ItemDetail} />
    </Tab.Navigator>
  );
}
