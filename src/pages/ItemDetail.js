import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import apiService from '../services/api.service';

export const ItemDetail = ({ route }) => {
  const [itemDetails, setItemDetails] = React.useState();

  const { id } = route.params;

  const getItemDetail = async itemId => {
    const data = await apiService.getItemDetail(itemId);
    setItemDetails(data);
  };

  React.useEffect(() => {
    getItemDetail(id);
  }, [id]);

  return (
    <View style={styles.container}>
      {itemDetails && (
        <View style={styles.innerContainer}>
          <Image source={{ uri: itemDetails.image }} style={styles.image} />
          <View style={styles.informationContainer}>
            <Text style={styles.name}>{itemDetails.name}</Text>
            <Text style={styles.description}>{itemDetails.description}</Text>
            <Text style={styles.price}>{`${itemDetails.price} $`}</Text>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
  innerContainer: { flex: 1, width: '100%', justifyContent: 'flex-start' },
  informationContainer: { marginHorizontal: 40 },
  image: {
    width: '100%',
    height: 200,
    alignSelf: 'center',
    marginVertical: 10,
  },
  name: { alignSelf: 'center', marginVertical: 15, fontSize: 25 },
  description: { fontSize: 20, fontStyle: 'italic', marginTop: 20 },
  price: { marginTop: 30, fontSize: 18 },
});
