import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export const Like = () => {
  return (
    <View style={styles.container}>
      <Text>Like screen</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
});
