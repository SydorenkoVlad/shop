import React, { useContext } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { CardsContainer } from '../components';

import apiService from '../services/api.service';
import { store } from '../store';

export const Home = () => {
  const [items, setItems] = React.useState([]);

  const {
    state: { isLoading },
  } = useContext(store);

  const getItems = async () => {
    try {
      const data = await apiService.getItems();
      setItems(data);
    } catch (err) {
      console.log('ERRRRRORR! ', err.message);
    }
  };

  React.useEffect(() => {
    getItems();
  }, [isLoading]);

  return (
    <View style={styles.container}>
      {isLoading && <Text>Loading...</Text>}
      {!!items && <CardsContainer items={items} />}
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', justifyContent: 'center' },
});
