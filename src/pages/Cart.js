import React, { useContext } from 'react';
import { ScrollView, View, Text, Pressable, StyleSheet } from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { store } from '../store';
import apiService from '../services/api.service';
import { CartItem, NoItemsInCart } from '../components';

export const Cart = () => {
  const globalState = useContext(store);
  const {
    dispatch,
    state: { cartItems },
  } = globalState;

  const clearCart = async () => {
    await apiService.resetCart(dispatch);
  };

  const totalSum = cartItems.reduce(
    (prev, current) => prev + current.price * current.quantity,
    0,
  );

  return (
    <View style={styles.container}>
      {!cartItems?.length ? (
        <NoItemsInCart />
      ) : (
        <>
          <View style={styles.cartHeaderContainer}>
            <Text style={styles.itemsTitle}>Items</Text>
            <Pressable onPress={clearCart}>
              <FontAwesome name="times" size={22} color="black" />
            </Pressable>
          </View>

          <ScrollView
            bounces={true}
            bouncesZoom={true}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            style={styles.scrollContainer}>
            <View style={styles.cartsContainer}>
              {cartItems.map((item, index) => (
                <CartItem
                  key={item.name}
                  isLast={index === cartItems.length - 1}
                  itemParams={item}
                  dispatch={dispatch}
                />
              ))}
            </View>
          </ScrollView>

          <View style={styles.totalSumContainer}>
            <Text style={styles.totalSumTitle}>Total sum:</Text>
            <Text style={styles.totalSumNumber}>{`${totalSum} $`}</Text>
          </View>

          <Pressable style={styles.proceedToBuyButton}>
            <Text style={styles.whiteText}>PROCEED TO BUY</Text>
          </Pressable>
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, marginHorizontal: 20 },
  cartHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 20,
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  itemsTitle: { fontSize: 25, fontWeight: 'bold' },
  cartsContainer: {
    flex: 1,
    paddingBottom: 16,
    justifyContent: 'center',
  },
  scrollContainer: { flex: 1 },
  totalSumContainer: { flexDirection: 'row', marginVertical: 10 },
  totalSumTitle: { fontSize: 19, fontWeight: 'bold' },
  totalSumNumber: { fontSize: 19, marginLeft: 15 },
  proceedToBuyButton: {
    backgroundColor: 'black',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginHorizontal: 70,
    marginVertical: 20,
  },
  whiteText: { color: 'white' },
});
