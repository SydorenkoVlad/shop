import axios from 'axios';

export const cartRequests = BASEURL => {
  const cartUrl = `${BASEURL}/cart`;

  return {
    addItemToCart: (id, quantity) =>
      axios.request({
        method: 'POST',
        url: `${cartUrl}/?id=${id}&quantity=${quantity}`,
      }),
    getAllCartItems: () =>
      axios.request({
        method: 'GET',
        url: cartUrl,
      }),
    resetCart: () =>
      axios.request({
        method: 'DELETE',
        url: cartUrl,
      }),
    deleteItemFromCart: id =>
      axios.request({
        method: 'DELETE',
        url: `${cartUrl}/${id}/`,
      }),
    updateItemInCart: (id, quantity) =>
      axios.request({
        method: 'PUT',
        url: `${cartUrl}/${id}/?quantity=${quantity}`,
      }),
  };
};
