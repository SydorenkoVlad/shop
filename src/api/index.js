import { cartRequests } from './cart';
import { itemsRequests } from './items';
import { env } from '../../env';

const { API_URL } = env;
export const API = {
  ...cartRequests(API_URL),
  ...itemsRequests(API_URL),
};
