import axios from 'axios';

export const itemsRequests = BASEURL => {
  const itemsUrl = `${BASEURL}/items`;

  return {
    getItems: () =>
      axios.request({
        method: 'GET',
        url: itemsUrl,
      }),
    getItemDetail: id =>
      axios.request({
        method: 'GET',
        url: `${itemsUrl}/${id}/`,
      }),
  };
};
