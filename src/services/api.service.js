import { API } from '../api';
import { apiMock } from '../storage';
import { env } from '../../env';

class ApiService {
  constructor() {
    this.isRealAPI = !!env.API_URL.length;
  }

  async getItems() {
    if (this.isRealAPI) {
      const result = await API.getItems();
      return result.data;
    }

    return apiMock.getItems();
  }

  async getAllCartItems() {
    if (this.isRealAPI) {
      const result = await API.getAllCartItems();
      return result.data;
    }

    return apiMock.getAllCartItems();
  }

  async getItemDetail(id) {
    if (this.isRealAPI) {
      const result = await API.getItemDetail(id);
      return result.data;
    }

    return apiMock.getItemDetail(id);
  }

  async addItemToCart(id, quantity, dispatch) {
    this.isRealAPI
      ? await API.addItemToCart(id, quantity)
      : await apiMock.addItemToCart(id, quantity);

    const data = await this.getAllCartItems();
    dispatch({ type: 'setCartItems', payload: data });
  }

  async updateItemInCart({ id, quantity, dispatch }) {
    this.isRealAPI
      ? await API.updateItemInCart(id, quantity)
      : await apiMock.updateItemInCart(id, quantity);

    const data = await this.getAllCartItems();
    dispatch({ type: 'setCartItems', payload: data });
  }

  async resetCart(dispatch) {
    this.isRealAPI ? await API.resetCart() : await apiMock.resetCart();
    dispatch({ type: 'setCartItems', payload: [] });
  }

  async deleteItemFromCart(id, dispatch) {
    this.isRealAPI
      ? await API.deleteItemFromCart(id)
      : await apiMock.deleteItemFromCart(id);

    const data = await this.getAllCartItems();
    dispatch({ type: 'setCartItems', payload: data });
  }
}

const apiService = new ApiService();

export default apiService;
