## Get Started

For Android:
1) install node_modules, run `yarn install` in project root.
2) then run `yarn android` in project root.

For iOS: 
1) install node_modules, run `yarn install` in project root.
2) go to `ios` folder and run `pod install`.
3) go back to project root and run `yarn ios`.