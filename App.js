import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import RootNavigation from './src/navigation';
import { StateProvider } from './src/store';

export default function App() {
  return (
    <StateProvider>
      <NavigationContainer>
        <RootNavigation />
      </NavigationContainer>
    </StateProvider>
  );
}
